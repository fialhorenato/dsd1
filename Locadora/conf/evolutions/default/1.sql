# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table cliente (
  codigo                    integer not null,
  nome                      varchar(255) not null,
  constraint pk_cliente primary key (codigo))
;

create table filme (
  codigo                    integer not null,
  nome                      varchar(255) not null,
  constraint pk_filme primary key (codigo))
;

create table locacao (
  codigo                    integer not null,
  cliente_codigo            integer not null,
  filme_codigo              integer not null,
  constraint pk_locacao primary key (codigo))
;

create sequence cliente_seq;

create sequence filme_seq;

create sequence locacao_seq;

alter table locacao add constraint fk_locacao_cliente_1 foreign key (cliente_codigo) references cliente (codigo) on delete restrict on update restrict;
create index ix_locacao_cliente_1 on locacao (cliente_codigo);
alter table locacao add constraint fk_locacao_filme_2 foreign key (filme_codigo) references filme (codigo) on delete restrict on update restrict;
create index ix_locacao_filme_2 on locacao (filme_codigo);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists cliente;

drop table if exists filme;

drop table if exists locacao;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists cliente_seq;

drop sequence if exists filme_seq;

drop sequence if exists locacao_seq;

