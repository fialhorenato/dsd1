package controllers;

import models.Cliente;
import models.Filme;
import models.Locacao;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

public class Locadora extends Controller {

	final static Form<Filme> filmesform = form(Filme.class);
	final static Form<Cliente> clientesform = form(Cliente.class);
	final static Form<Locacao> locacaoform = form(Locacao.class);

	public static Result index() {
		return ok(index.render());
	}

	public static Result indexFilmes() {
		return ok(filmes.render(Filme.listarTodos()));
	}
	
	public static Result indexClientes() {
		return ok(clientes.render(Cliente.listarTodos()));
	}
	
	public static Result indexLocacoes() {
		return ok(locacao.render(Cliente.listarTodos(), Filme.listarTodos(), Locacao.listarTodos() ));
	}
	

	public static Result criaFilme() {
		Form<Filme> formularioCompleto = filmesform.bindFromRequest();
		formularioCompleto.get().save();
		return indexFilmes();
	}

	public static Result deletaFilme() {
		Form<Filme> formularioCompleto = filmesform.bindFromRequest();
		int codigo = formularioCompleto.bindFromRequest().get().codigo;
		Filme.deleta(codigo);
		return indexFilmes();
	}
	
	public static Result criaLocacao() {
		Form<Locacao> formularioCompleto = locacaoform.bindFromRequest();
		int codigoDoFilme = Integer.valueOf(formularioCompleto.field("filmes").value());
		int codigodoCliente = Integer.valueOf(formularioCompleto.field("clientes").value());
		Locacao l = new Locacao();
		l.cliente = Cliente.find.byId(codigodoCliente);
		l.filme = Filme.find.byId(codigoDoFilme);
		l.save();
		return indexLocacoes();
	}
	
	public static Result deletaLocacao() {
		Form<Locacao> formularioCompleto = locacaoform.bindFromRequest();
		int codigo = formularioCompleto.bindFromRequest().get().codigo;
		Locacao.deleta(codigo);
		return indexLocacoes();
	}

	public static Result criaCliente() {
		Form<Cliente> formularioCompleto = clientesform.bindFromRequest();
		formularioCompleto.get().save();
		return indexClientes();
	}

	public static Result deletaCliente() {
		Form<Cliente> formularioCompleto = clientesform.bindFromRequest();
		int codigo = formularioCompleto.bindFromRequest().get().codigo;
		Cliente.deleta(codigo);
		return indexClientes();
	}

}
