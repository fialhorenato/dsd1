package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.validation.NotNull;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Filme extends Model {
	
	public static Finder<Integer, Filme> find = new Finder<>(Integer.class, Filme.class);
	
	@Id
	public int codigo;
	
	@NotNull
	public String nome;
	
	public static List<Filme> listarTodos() {
		return find.all();
	}
	
	public static Filme buscarPorId(int id) {
		return find.byId(id);
	}
	
	public static void deleta(int id) {
		Filme.find.byId(id).delete();
	}

	@Override
	public String toString() {
		return "Nome = " + nome + "\n" +
				"Codigo = " + codigo + "\n";
		
	}

}
