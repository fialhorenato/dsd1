package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.avaje.ebean.validation.NotNull;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Locacao extends Model {

	public static Finder<Integer, Locacao> find = new Finder<>(Integer.class,
			Locacao.class);

	@Id
	public int codigo;

	@ManyToOne
	@NotNull
	public Cliente cliente;

	@ManyToOne
	@NotNull
	public Filme filme;

	public static List<Locacao> listarTodos() {
		return find.all();
	}
	
	public static void deleta(int id) {
		Locacao.find.byId(id).delete();
	}

	public String toString() {
		return "Codigo = " + this.codigo + "\n" + cliente.toString()
				+ filme.toString();
	}

}
