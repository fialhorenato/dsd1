package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.validation.NotNull;

import play.db.ebean.Model;

@Entity
public class Cliente extends Model {
	
	public static Finder<Integer, Cliente> find = new Finder<>(Integer.class, Cliente.class);
	
	@Id
	public int codigo;
	
	@NotNull
	public String nome;
	
	public static List<Cliente> listarTodos() {
		return find.all();
	}
	
	public Cliente buscarPorId(int id) {
		return find.byId(id);
	}

	public static void deleta(int id) {
		Cliente.find.byId(id).delete();
	}
	
	@Override
	public String toString() {
		return "Nome = " + nome + "\n" +
				"Codigo = " + codigo + "\n";
		
	}
	
	
}
