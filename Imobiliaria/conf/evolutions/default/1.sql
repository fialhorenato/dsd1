# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table compra (
  id                        integer not null,
  imovel_id                 integer not null,
  comprador_id              integer not null,
  constraint pk_compra primary key (id))
;

create table comprador (
  id                        integer not null,
  nome                      varchar(255) not null,
  cpf                       varchar(255) not null,
  constraint pk_comprador primary key (id))
;

create table imovel (
  id                        integer not null,
  endereco                  varchar(255) not null,
  tipo                      varchar(255) not null,
  constraint pk_imovel primary key (id))
;

create sequence compra_seq;

create sequence comprador_seq;

create sequence imovel_seq;

alter table compra add constraint fk_compra_imovel_1 foreign key (imovel_id) references imovel (id) on delete restrict on update restrict;
create index ix_compra_imovel_1 on compra (imovel_id);
alter table compra add constraint fk_compra_comprador_2 foreign key (comprador_id) references comprador (id) on delete restrict on update restrict;
create index ix_compra_comprador_2 on compra (comprador_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists compra;

drop table if exists comprador;

drop table if exists imovel;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists compra_seq;

drop sequence if exists comprador_seq;

drop sequence if exists imovel_seq;

