package controllers;

import java.util.List;

import com.avaje.ebean.Query;

import models.Compra;
import models.Comprador;
import models.Imovel;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Controller;

import views.html.*;

public class Imobiliaria extends Controller {

	final static Form<Imovel> imovelForm = form(Imovel.class);
	final static Form<Comprador> compradorForm = form(Comprador.class);
	final static Form<Compra > compraForm = form(Compra.class);
	
	public static Result index() {
		return ok(index.render());
	}

	public static Result imovelIndex() {
		return ok(imovel.render(Imovel.all(), Comprador.all()));
	}

	public static Result compradorIndex() {
		return ok(comprador.render(compradorForm, Comprador.all()));
	}
	
	public static Result compraIndex() {
		return ok(compra.render(Imovel.all(), Comprador.all(), Compra.all()));
	}

	public static Result salvaImovel() {
		Form<Imovel> formularioCheio = imovelForm.bindFromRequest();
		formularioCheio.get().save();
		return imovelIndex();
	}
	
	public static Result salvaCompra() {
		int idimovel = Integer.parseInt(compraForm.bindFromRequest().field("imoveis").value());
		int idcomprador = Integer.parseInt(compraForm.bindFromRequest().field("compradores").value());
		Compra c = new Compra();
		c.comprador = Comprador.find.byId(idcomprador);
		c.imovel = Imovel.find.byId(idimovel);
		c.save();
		return compraIndex();
	}


	public static Result deletaImovel(int id) {
		Imovel.delete(id);
		return imovelIndex();
	}
	
	public static Result deletaCompra(int id) {
		Compra.delete(id);
		return compraIndex();
	}

	public static Result adicionaComprador() {
		Form<Comprador> formularioCheio = compradorForm.bindFromRequest();
		formularioCheio.get().save();
		return compradorIndex();
	}

	public static Result deletaComprador(int id) {
		Comprador.delete(id);
		return compradorIndex();
	}

}
