package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import play.db.ebean.Model;

import com.avaje.ebean.validation.NotNull;

@Entity
public class Imovel extends Model {

	private static final long serialVersionUID = 2640544381762818207L;

	public static Finder<Integer, Imovel> find = new Finder(Integer.class,
			Imovel.class);

	@Id
	public int id;

	@NotNull
	public String endereco;

	@NotNull
	public String tipo;


	public String toString() {
		String result = "";
		result += "Id =  " + this.id + "\n";
		result += "Endereco = " + this.endereco + "\n";
		result += "Tipo = " + this.tipo + "\n";
		return result;
	}

	public static List<Imovel> all() {
		return find.all();
	}

	public static void delete(int id) {
		Imovel i = find.byId(id);
		i.delete();
	}

}
