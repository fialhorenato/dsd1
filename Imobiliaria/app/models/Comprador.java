package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.validation.NotNull;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Comprador extends Model {
	
	public static Finder<Integer, Comprador> find = new Finder(Integer.class, Comprador.class);
	
	@Id
	public int id;

    @NotNull
	public String nome;

    @NotNull
	public String cpf;
	
	
	public static List<Comprador> all() {
		return find.all();
	}
	
	
	
	public static void delete(int id) {
		Comprador c = find.byId(id);
		c.delete();
	}
	
	public String toString() {
		String result = "";
		result += "Id =  " + this.id + "\n";
		result += "Nome = " + this.nome + "\n";
		result += "Cpf = " + this.cpf + "\n";
		return result;
	}
	
}
