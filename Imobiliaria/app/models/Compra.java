package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.avaje.ebean.validation.NotNull;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Compra extends Model {
	
	public final static Finder<Integer,Compra> find = new Finder<>(Integer.class, Compra.class);
	
	@Id
	public int id;
	
	@NotNull
	@ManyToOne
	public Imovel imovel;

	@NotNull
	@ManyToOne
	public Comprador comprador;
	
	public static List<Compra> all() {
		return find.all();
	}
	
	public static void delete(int id) {
		Compra i = find.byId(id);
		i.delete();
	}

}
