# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table comentario (
  id                        bigint not null,
  nome                      varchar(255) not null,
  comentario                varchar(255) not null,
  post_id                   bigint not null,
  constraint pk_comentario primary key (id))
;

create table post (
  id                        bigint not null,
  assunto                   varchar(255) not null,
  conteudo                  varchar(255) not null,
  autor_id                  bigint not null,
  constraint pk_post primary key (id))
;

create table usuario (
  id                        bigint not null,
  nome                      varchar(255),
  constraint pk_usuario primary key (id))
;

create sequence comentario_seq;

create sequence post_seq;

create sequence usuario_seq;

alter table comentario add constraint fk_comentario_post_1 foreign key (post_id) references post (id) on delete restrict on update restrict;
create index ix_comentario_post_1 on comentario (post_id);
alter table post add constraint fk_post_autor_2 foreign key (autor_id) references usuario (id) on delete restrict on update restrict;
create index ix_post_autor_2 on post (autor_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists comentario;

drop table if exists post;

drop table if exists usuario;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists comentario_seq;

drop sequence if exists post_seq;

drop sequence if exists usuario_seq;

