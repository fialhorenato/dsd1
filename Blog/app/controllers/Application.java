package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Comentario;
import models.Post;
import models.Usuario;
import play.*;
import play.data.Form;
import play.mvc.*;

import views.html.*;
import views.html.page;

public class Application extends Controller {

	private final static Form<Post> formPost = form(Post.class);
	private final static Form<Comentario> formComentario = form(Comentario.class);
	private final static Form<Usuario> formUsuario = form(Usuario.class);

	public static Result index() {
		return ok(index.render(Post.find.all()));
	}

	public static Result criarPost() {
		return ok(criarpost.render(Usuario.find.all()));
	}

	public static Result salvaPost() {
		Form<Post> formpreenchido = formPost.bindFromRequest();
		Post p = formpreenchido.get();
		Long userid = Long.valueOf(formpreenchido.field("users").value());
		Usuario u = Usuario.find.byId(userid);
		p.autor = u;
		p.save();
		return index();
	}

	public static Result criarUsuario() {
		return ok(usuario.render(Usuario.find.all()));
	}

	public static Result salvarUsuario() {
		formUsuario.bindFromRequest().get().save();
		return index();
	}

	public static Result abrePost(Long id) {
		List<Comentario> list = new ArrayList<Comentario>();
		Post p = Post.find.byId(id);
		
		if (!list.isEmpty()) {
			for (Comentario c : list) {
				if (c.post.id == id) {
					list.add(c);
				}
			}
		}

		return ok(page.render(p, list));

	}

	public static Result salvaComentario(Long id) {
		Comentario c = formComentario.bindFromRequest().get();
		c.post = Post.find.byId(id);
		c.save();
//		return abrePost(id);
		return ok(c.toString());
	}

}