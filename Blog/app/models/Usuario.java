package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Usuario extends Model{
	
	private static final long serialVersionUID = -4236739774842992594L;

	public final static Finder<Long, Usuario> find = new Finder<>(Long.class, Usuario.class);
	
	@Id
	public Long id;
	
	@Required
	public String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	
}
