package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.avaje.ebean.validation.NotNull;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Comentario extends Model {

	private static final long serialVersionUID = 2149838513427052121L;


	public static Finder<Long, Comentario> find = new Finder<>(Long.class, Comentario.class);
	
	
	@Id
	public Long id;
	
	@NotNull
	public String nome;
	
	@NotNull
	public String comentario;
	
	@NotNull
	@ManyToOne
	public Post post;
}
