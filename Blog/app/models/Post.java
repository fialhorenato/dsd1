package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.avaje.ebean.validation.NotNull;

import play.db.ebean.Model;

@Entity
public class Post extends Model {
	
	private static final long serialVersionUID = -3052453533688124940L;

	public static Finder<Long, Post> find = new Finder<>(Long.class, Post.class);
	
	@Id
	public Long id;

	@NotNull
	public String assunto;
	
	@NotNull
	public String conteudo;
	
	@NotNull
	@ManyToOne
	public Usuario autor;
}
